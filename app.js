var MongoClient = require('mongodb').MongoClient
    , assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/myproject';
// Use connect method to connect to the server

var recipe = require("./recipe.json")

MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
    insertRecipe(db, recipe, function() {
        db.close();
    });
    findRecipe(db, function() {
        db.close();
    });
    console.log("Disconnecting")
});

var insertRecipe = function(db, json, callback) {
    // Get the documents collection
    var collection = db.collection('recipes');
    // Insert some documents
    collection.save(json, function(err, result) {
        assert.equal(err, null);
        console.log("Inserted the json into the collection");
        callback(result);
    });
}

var findRecipe = function(db, callback) {
    // Get the documents collection
    var collection = db.collection('recipes');
    // Find all the records
    collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.log(JSON.stringify(docs));
        callback(docs);
    })
}

